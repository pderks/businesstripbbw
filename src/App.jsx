import React, { useState, useEffect } from "react";
import "./App.css";
import Footer from "./Footer";
import Header from "./Header";

export default function App() {
  // State zum Speichern der Trips und des ausgewählten Trip-IDs
  const [trips, setTrips] = useState([]);
  const [selectedTripId, setSelectedTripId] = useState("");
  const [newTrip, setNewTrip] = useState({ title: "", tripOrFlight: 'trip', description: "", startTrip: "", endTrip: "" });
  const [editingTrip, setEditingTrip] = useState(null);

  // useEffect-Hook, um die Trips beim Laden der Komponente zu laden
  useEffect(() => {
    fetchTrips();
  }, []);

  // Funktion zum Laden der Trips vom JSON Server
  const fetchTrips = async () => {
    const response = await fetch('http://localhost:3001/trips');
    const data = await response.json();
    setTrips(data);
  };

  // Funktion zum Hinzufügen eines neuen Trips
  const addTrip = async (e) => {
    e.preventDefault();
    const response = await fetch('http://localhost:3001/trips', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newTrip)
    });
    const newTripResponse = await response.json();
    setTrips([...trips, newTripResponse]);
    setNewTrip({ title: "", tripOrFlight: "trip", description: "", startTrip: "", endTrip: "" });
  };

  // Funktion zum Aktualisieren eines bestehenden Trips
  const updateTrip = async (e) => {
    e.preventDefault();
    const response = await fetch(`http://localhost:3001/trips/${editingTrip.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(editingTrip)
    });
    const updatedTrip = await response.json();
    setTrips(trips.map(trip => trip.id === updatedTrip.id ? updatedTrip : trip));
    setEditingTrip(null);
  };

  // Funktion zum Löschen eines Trips
  const deleteTrip = async (id) => {
    await fetch(`http://localhost:3001/trips/${id}`, {
      method: 'DELETE'
    });
    setTrips(trips.filter(trip => trip.id !== id));
  };

  // Event-Handler für Änderungen in der Dropdown-Auswahl
  const handleDropdownChange = (event) => {
    setSelectedTripId(event.target.value);
  };

  // Event-Handler für die Änderungen im Formular
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setNewTrip({ ...newTrip, [name]: value });
  };

  const handleEditInputChange = (e) => {
    const { name, value } = e.target;
    setEditingTrip({ ...editingTrip, [name]: value });
  };

  const startEditTrip = (trip) => {
    setEditingTrip(trip);
  };

  // Funktion zum Rendern eines einzelnen Trips
  const renderTrip = (t) => (
    <div className="product" key={t.id}>
      <figure>
        <div>
          <img src={"images/items/" + t.id + ".jpg"} alt={t.title} />
        </div>
        <figcaption>
          <a href="#">{t.title}</a>
          <div>
            <span>
              {t.startTrip[2] + "-" + t.startTrip[1] + "-" + t.startTrip[0]}
            </span>
          </div>
          <p>{t.description}</p>
          <div>
            <button type="button" onClick={() => deleteTrip(t.id)}>
              Delete Trip/Flight
            </button>
            <button type="button" onClick={() => startEditTrip(t)}>
              Edit Trip/Flight
            </button>
          </div>
        </figcaption>
      </figure>
    </div>
  );

  // Filtert die Trips basierend auf der ausgewählten Trip-ID
  const filteredTrips = selectedTripId
    ? trips.filter((trip) => trip.id === parseInt(selectedTripId))
    : trips;

  return (
    <>
      <div>
        <Header />
        <main>
          <section id="controls">

            {editingTrip && (
              <button onClick={() => setEditingTrip(null)}>
                Cancel Edit
              </button>
            )}
          </section>

          <section id="create-update">
            {editingTrip ? (
              <>
                <h2>Edit Trip</h2>
                <form onSubmit={updateTrip}>
                  <input
                    type="text"
                    name="title"
                    placeholder="Title"
                    value={editingTrip.title}
                    onChange={handleEditInputChange}
                    required
                  />
                  <select
                    name="tripOrFlight"
                    value={editingTrip.tripOrFlight}
                    onChange={handleEditInputChange}
                  >
                    <option value="trip">Trip</option>
                    <option value="flight">Flight</option>
                  </select>
                  <input
                    type="text"
                    name="description"
                    placeholder="Description"
                    value={editingTrip.description}
                    onChange={handleEditInputChange}
                    required
                  />
                  <input
                    type="text"
                    name="startTrip"
                    placeholder="Start Trip (YYYY-MM-DD)"
                    value={editingTrip.startTrip}
                    onChange={handleEditInputChange}
                    required
                  />
                  <input
                    type="text"
                    name="endTrip"
                    placeholder="End Trip (YYYY-MM-DD)"
                    value={editingTrip.endTrip}
                    onChange={handleEditInputChange}
                    required
                  />
                  <button type="submit">Update Trip</button>
                </form>
              </>
            ) : (
              <>
                <h2>Add a New Trip or a New Flight</h2>
                <form onSubmit={addTrip}>
                  <input
                    type="text"
                    name="title"
                    placeholder="Title"
                    value={newTrip.title}
                    onChange={handleInputChange}
                    required
                  />
                  <select
                    name="tripOrFlight"
                    value={newTrip.tripOrFlight}
                    onChange={handleInputChange}
                  >
                    <option value="trip">Trip</option>
                    <option value="flight">Flight</option>
                  </select>
                  <input
                    type="text"
                    name="description"
                    placeholder="Description"
                    value={newTrip.description}
                    onChange={handleInputChange}
                    required
                  />
                  <input
                    type="text"
                    name="startTrip"
                    placeholder="Start Trip (YYYY-MM-DD)"
                    value={newTrip.startTrip}
                    onChange={handleInputChange}
                    required
                  />
                  <input
                    type="text"
                    name="endTrip"
                    placeholder="End Trip (YYYY-MM-DD)"
                    value={newTrip.endTrip}
                    onChange={handleInputChange}
                    required
                  />
                  <button type="submit">Add Trip</button>
                </form>
              </>
            )}
          </section>

          <section id="filters">
            <label htmlFor="trip">Select a Trip:</label>{" "}
            <select id="dropdown" onChange={handleDropdownChange}>
              <option value="">All Trips/Flights</option>
              {trips.map((trip) => (
                <option key={trip.id} value={trip.id}>
                  {trip.title}
                </option>
              ))}
            </select>
          </section>

          <section id="products">{filteredTrips.map(renderTrip)}</section>
        </main>
      </div>
      <Footer />
    </>
  );
}
