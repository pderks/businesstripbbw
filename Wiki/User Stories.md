# User Stories

#### Trip hinzufügen

User kann einen neuen Trip über ein Formular hinzufügen, um eine bevorstehende Geschäftsreise zu registrieren.

#### Trip bearbeiten

User kann die Details eines bestehenden Trips bearbeiten, um Änderungen oder Korrekturen an einer Reise vorzunehmen.

#### Trip löschen

User kann einen bestehenden Trip löschen, um nicht mehr benötigte oder falsch erstellte Reisen zu entfernen.

#### Trip anzeigen

User kann eine Liste aller Trips anzeigen, um einen Überblick über geplante und vergangene Reisen zu haben.

#### Trip filtern

User kann Trips basierend auf einer Dropdown-Auswahl filtern, um spezifische Reisen schnell zu finden.

#### Formularvalidierung

User kann sicherstellen, dass alle erforderlichen Felder im Formular ausgefüllt sind, bevor ein neuer Trip hinzugefügt oder ein bestehender Trip bearbeitet wird, um sicherzustellen, dass alle notwendigen Informationen bereitgestellt werden.