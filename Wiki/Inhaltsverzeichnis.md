# Hauptseite

Willkommen auf der Hauptseite unseres Projekt-Wikis.

## Abschnitte

1. [Idee](Idee)
2. [User Stories](user-stories)
3. [Code](code)

## Idee

Hier ist ein Link zur Seite [Idee](Idee).

## User Stories

Detaillierte Benutzerstories finden Sie auf der Seite [User Stories](user-stories).

## Code

Den Quellcode finden Sie auf der Seite [Code](code).